/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoInternalTransactionStatusBatch.config;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author emarmole
 */
@Configuration
public class ConfigurationProperties 
{   
    private static final Logger logger = LogManager.getLogger(ConfigurationProperties.class);

    public Object getConfig(String key, Object obj, Class clase) throws Exception
    {
        logger.info("INICIANDO LECTURA DE CONFIGURACIÓN OBJETO -> "+key);
        Map<String, Object> objConfig;
        String config = getProperties();
        Gson gson = new Gson();
        objConfig = gson.fromJson(config, Map.class);
        obj = gson.fromJson(gson.toJson(objConfig.get(key)), clase);
                
        return obj;
    }
    
    public String getProperties() throws Exception
    {
        String jsonProperties = "";
        try {
            //InputStream jsonConfig = ConfigurationProperties.class.getClassLoader().getResourceAsStream("NovoIncExtTransactionStatusConfiguration.json");
            File jsonConfig = new File(System.getProperty("user.dir")+"/config/NovoInternalTransactionStatusConfiguration.json");
            jsonProperties = readFromInputStream(jsonConfig);
            return jsonProperties;
        } catch (Exception e) {
            throw e;
        }
    }
    
    private String readFromInputStream(File inputStream) throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }
    
}

