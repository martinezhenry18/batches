package com.novo.NovoInternalTransactionStatusBatch.controller;

import com.novo.NovoInternalTransactionStatusBatch.Objects.ApiResponseAutenticateObject;
import com.novo.NovoInternalTransactionStatusBatch.Objects.ApiResponseDecisionTransactionObject;
import com.novo.NovoInternalTransactionStatusBatch.Objects.BodyNotificationObject;
import com.novo.NovoInternalTransactionStatusBatch.Objects.ExternalTransactionsObject;
import com.novo.NovoInternalTransactionStatusBatch.Objects.GeneralParametersObject;
import com.novo.NovoInternalTransactionStatusBatch.Objects.MsAlertsObject;
import com.novo.NovoInternalTransactionStatusBatch.Objects.ReportContExtTransactionObject;
import com.novo.NovoInternalTransactionStatusBatch.Objects.TransactionDesicionObject;
import com.novo.NovoInternalTransactionStatusBatch.Objects.TransactionObject;
import com.novo.NovoInternalTransactionStatusBatch.Objects.TransactionResponseObject;
import com.novo.NovoInternalTransactionStatusBatch.components.ListComponentsComponent;
import com.novo.NovoInternalTransactionStatusBatch.config.ConfigurationProperties;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import com.novo.NovoInternalTransactionStatusBatch.service.NovoInternalDecisionTransactionService;
import com.novo.NovoInternalTransactionStatusBatch.service.NovoInternalTransactionsListService;
import com.novo.NovoInternalTransactionStatusBatch.service.NovoInternalNotificationCenterService;
import com.novo.NovoInternalTransactionStatusBatch.service.NovoInternalNiceConsumeService;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author emarmole
 */
@Component
public class InternalTransactionsStatusController 
{
    private static final Logger logger = LogManager.getLogger(InternalTransactionsStatusController.class);
    
    @Autowired
    private NovoInternalTransactionsListService novoInternalTransactionsListService;
    
    
    private ListComponentsComponent listComponentsComponent;
    
    @Autowired
    private NovoInternalNotificationCenterService novoInternalNotificationCenterService;
    
    @Autowired
    private NovoInternalNiceConsumeService novoInternalNiceConsumeService;
    
    @Autowired
    private NovoInternalDecisionTransactionService novoInternalDecisionTransactionService;
    
    
    @Bean
    public void internalTransactionStatus() throws Exception
    {
        ConfigurationProperties config = new ConfigurationProperties();
        Map<String, String> transactionStatus = new HashMap<String, String>();
        transactionStatus = (Map<String, String>) config.getConfig("transactionStatusNice", transactionStatus, HashMap.class);
        
        Map<String, String> defineStatus = new HashMap<String, String>();
        defineStatus = (Map<String, String>) config.getConfig("defineStstuaTransactions", defineStatus, HashMap.class);
        
        
        ReportContExtTransactionObject objReport = new ReportContExtTransactionObject();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        GeneralParametersObject objGeneralParameters = new GeneralParametersObject();
        MsAlertsObject objMsAlerts = new MsAlertsObject();        
        ApiResponseDecisionTransactionObject objResponseDecision;
        TransactionDesicionObject objTransactionDecisionObject;
        ApiResponseAutenticateObject objResponseAutenticate;
        objMsAlerts = (MsAlertsObject) config.getConfig("msAlertMessages", objMsAlerts, MsAlertsObject.class);
        String systemId = "";
        
        novoInternalNotificationCenterService.internalTransactionsNotification("INTERNAL TRANSACTION FRAUD PROCESS INIT", "info");
        
        objGeneralParameters = (GeneralParametersObject) config.getConfig("generalParameters", objGeneralParameters, GeneralParametersObject.class);
        
        systemId = objGeneralParameters.getSystemId();
        
        ArrayList<TransactionObject> listTransactions = novoInternalTransactionsListService.getListTransactions();
        if (listTransactions.size() > 0) 
        {
            objReport.setTotalTransactions(listTransactions.size());
            if (!systemId.isEmpty()) 
            {
                listTransactions = listComponentsComponent.addSystemIdToTransactions(listTransactions, systemId);
            }            
            try {
                objResponseAutenticate = novoInternalNiceConsumeService.consumeWebRestService();
                
                if (objResponseAutenticate.isLogin()) 
                {
                    ExternalTransactionsObject objExternalTransactionsObject = new ExternalTransactionsObject();
                    objExternalTransactionsObject.setCustomerID(objGeneralParameters.getCustomerId());
                    objExternalTransactionsObject.setVersion(objGeneralParameters.getVersion());
                    objExternalTransactionsObject.setTransactions(listTransactions);
                    ArrayList<TransactionResponseObject> listResponse = novoInternalNiceConsumeService.consumeTransactionApiFraud(objResponseAutenticate, objExternalTransactionsObject);
                    
                    for (TransactionResponseObject transactionResponseObject : listResponse) 
                    {
                        String status = Optional.ofNullable(transactionStatus.get(transactionResponseObject.getResult())).orElse("Unknown");;
                        int idStatus = Integer.parseInt(defineStatus.get(status));
                        String idTransaction = transactionResponseObject.getTransactionID().split("_")[1];
                        switch (idStatus)
                        {
                            case 1:
                                objReport.setHoldTransactions(objReport.getHoldTransactions() + 1);
                                break;
                            case 2:
                                objResponseDecision = new ApiResponseDecisionTransactionObject();
                                
                                objTransactionDecisionObject = new TransactionDesicionObject();
                                objTransactionDecisionObject.setReason("REJECT TRANSACTION FROM NICE ACTIMIZE");
                                objTransactionDecisionObject.setFraudHoldResolutionDate(format.format(new Date()));
                                objTransactionDecisionObject.setTransactionRefId(idTransaction);
                                
                                
                                objResponseDecision = novoInternalDecisionTransactionService.rejectTransaction(idTransaction, objTransactionDecisionObject, objGeneralParameters.getTenant());
                                
                                if (!objResponseDecision.getRc().equals("0")) 
                                {
                                    objReport.setFailedApproveTransactions(objReport.getFailedCancelTransactions() + 1);
                                    novoInternalNotificationCenterService.internalTransactionsNotification("Fallo el rechazo de la transacción #"+idTransaction, "error");
                                } else {
                                    objReport.setCancelTransactions(objReport.getCancelTransactions() + 1);
                                }
                                break;
                            case 3:
                                objResponseDecision = new ApiResponseDecisionTransactionObject();
                                
                                objResponseDecision = novoInternalDecisionTransactionService.approveTransaction(idTransaction, objGeneralParameters.getTenant());
                                
                                if (!objResponseDecision.getRc().equals("0")) 
                                {
                                    objReport.setFailedApproveTransactions(objReport.getFailedApproveTransactions() + 1);
                                    novoInternalNotificationCenterService.internalTransactionsNotification("Fallo la aprobación de la transacción #"+idTransaction, "error");
                                } else {
                                    objReport.setReleaseTransactions(objReport.getReleaseTransactions() + 1);
                                }
                                
                                break;
                            case 4:
                                objReport.setUnknownTransactions(objReport.getUnknownTransactions() + 1);
                                break;
                        }
                    }
                    
                    
                    Map<String, Object> params = new HashMap<>();
                    BodyNotificationObject objBody = new BodyNotificationObject(listTransactions, listResponse);
                    params.put("TRANSACTIONS_LIST", objBody.getTransactionList());
                    novoInternalNotificationCenterService.internalTransactionsNotificationExternal(params, objGeneralParameters.getTenant());
                } else {
                    novoInternalNotificationCenterService.internalTransactionsNotification(objMsAlerts.getFailedNiceAuth(), "error");
                }
            } catch (Exception e) {
                e.printStackTrace();
                novoInternalNotificationCenterService.internalTransactionsNotification(objMsAlerts.getFailedGetTransactions(), "error");
            }                        
        } else {
            novoInternalNotificationCenterService.internalTransactionsNotification(objMsAlerts.getNoTransactions(), "info");
        }
        String textFinal = "";
        textFinal  = "<ul><li><b>Summary:</b></li>";
        textFinal += "<ul><li>Total Transactions: "+Integer.toString(objReport.getTotalTransactions())+"</li>";
        textFinal += "<li>Hold: "+Integer.toString(objReport.getHoldTransactions())+"</li>";
        textFinal += "<li>Cancel: "+Integer.toString(objReport.getCancelTransactions())+"</li>";
        textFinal += "<li>Approve: "+Integer.toString(objReport.getReleaseTransactions())+"</li>";
        textFinal += "<li>Failed Approve: "+Integer.toString(objReport.getFailedApproveTransactions())+"</li>";
        textFinal += "<li>Failed Cancel: "+Integer.toString(objReport.getFailedCancelTransactions())+"</li></ul></ul>";
        novoInternalNotificationCenterService.internalTransactionsNotification(textFinal, "success");
    }
}
