/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoInternalTransactionStatusBatch.repository;

import com.novo.NovoInternalTransactionStatusBatch.Objects.TransactionObject;
import java.util.ArrayList;

/**
 *
 * @author emarmole
 */
public interface NovoInternalTransactionListRepository 
{
    public ArrayList<TransactionObject> getListTransactions() throws Exception;
}
