/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoInternalTransactionStatusBatch.repository.impl;

import com.novo.NovoInternalTransactionStatusBatch.Objects.GeneralProcedureDbObject;
import com.novo.NovoInternalTransactionStatusBatch.Objects.MsAlertsObject;
import com.novo.NovoInternalTransactionStatusBatch.Objects.ProcedureDbObject;
import com.novo.NovoInternalTransactionStatusBatch.Objects.TransactionObject;
import com.novo.NovoInternalTransactionStatusBatch.config.ConfigurationProperties;
import com.novo.NovoInternalTransactionStatusBatch.repository.NovoInternalTransactionListRepository;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

/**
 *
 * @author emarmole
 */
@Repository
public class NovoInternalTransactionsListRepositoryImpl extends GeneralRepositoryImpl implements NovoInternalTransactionListRepository
{
    private static final Logger logger = LogManager.getLogger(NovoInternalTransactionsListRepositoryImpl.class);
    
    @Autowired
    private ConfigurationProperties configProps;
    
    @Override
    public ArrayList<TransactionObject> getListTransactions() throws Exception
    {
        OracleDataSource dataSource = getConnection();
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        GeneralProcedureDbObject obj = new GeneralProcedureDbObject();
        ProcedureDbObject objProcedureDbObject;
        obj = (GeneralProcedureDbObject) configProps.getConfig("dataProcedureTransaction", obj, GeneralProcedureDbObject.class);
        objProcedureDbObject = obj.getListTransactionsEscalate();
        
        String sql = objProcedureDbObject.getDbProcedure();
        String pkg = objProcedureDbObject.getDbPackage();
        ArrayList<TransactionObject> listCustomers = new ArrayList<>();
        try {
            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withCatalogName(pkg).withProcedureName(sql)
                    .returningResultSet("TRANSACTIONS", new CostumerMapperUtil());
            Map<String, Object> out = simpleJdbcCall.execute();       
            listCustomers = (ArrayList<TransactionObject>) out.get("TRANSACTIONS");
            return listCustomers;
        } catch (Exception e) {
            //riskNotificationCenterService.riskNotification(objMsAlertsObject.getFailedGetCustomers(), "error");
            e.printStackTrace();
            return listCustomers;
        }
    }
    
    private static class CostumerMapperUtil implements RowMapper<TransactionObject>
    {
        @Override
        public TransactionObject mapRow(ResultSet rs, int i) throws SQLException {
            return new TransactionObject(rs.getString("transactionId"), "internal");
        }        
    }
}
