/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoInternalTransactionStatusBatch.service;

import com.novo.NovoInternalTransactionStatusBatch.Objects.ApiResponseDecisionTransactionObject;
import com.novo.NovoInternalTransactionStatusBatch.Objects.TransactionDesicionObject;

/**
 *
 * @author emarmole
 */
public interface NovoInternalDecisionTransactionService 
{
    public ApiResponseDecisionTransactionObject approveTransaction(String idTransaction, String tenant);
    public ApiResponseDecisionTransactionObject rejectTransaction(String idTransaction,TransactionDesicionObject objTransaction, String tenant);
}
