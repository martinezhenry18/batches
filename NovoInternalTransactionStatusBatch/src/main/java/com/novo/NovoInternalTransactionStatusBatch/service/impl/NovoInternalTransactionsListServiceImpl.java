/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoInternalTransactionStatusBatch.service.impl;

import com.novo.NovoInternalTransactionStatusBatch.Objects.TransactionObject;
import com.novo.NovoInternalTransactionStatusBatch.repository.NovoInternalTransactionListRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.novo.NovoInternalTransactionStatusBatch.service.NovoInternalTransactionsListService;

/**
 *
 * @author emarmole
 */
@Service
public class NovoInternalTransactionsListServiceImpl implements NovoInternalTransactionsListService
{

    @Autowired
    private NovoInternalTransactionListRepository novoInternalTransactionListRepository;
    
    @Override
    public ArrayList<TransactionObject> getListTransactions() throws Exception {
        return novoInternalTransactionListRepository.getListTransactions();
    }
    
}
