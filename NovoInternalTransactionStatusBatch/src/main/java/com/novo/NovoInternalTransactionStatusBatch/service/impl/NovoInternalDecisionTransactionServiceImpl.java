/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoInternalTransactionStatusBatch.service.impl;

import com.google.gson.Gson;
import com.novo.NovoInternalTransactionStatusBatch.Objects.ApiResponseDecisionTransactionObject;
import com.novo.NovoInternalTransactionStatusBatch.Objects.ApproveTransactionObject;
import com.novo.NovoInternalTransactionStatusBatch.Objects.RejectTransactionObject;
import com.novo.NovoInternalTransactionStatusBatch.Objects.TransactionDesicionObject;
import com.novo.NovoInternalTransactionStatusBatch.config.ConfigurationProperties;
import com.novo.NovoInternalTransactionStatusBatch.constant.HeaderParams;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.novo.NovoInternalTransactionStatusBatch.service.NovoInternalDecisionTransactionService;

/**
 *
 * @author emarmole
 */
@Service
public class NovoInternalDecisionTransactionServiceImpl implements NovoInternalDecisionTransactionService
{
    private static final Logger logger = LogManager.getLogger(NovoInternalDecisionTransactionServiceImpl.class);
    
    @Override
    public ApiResponseDecisionTransactionObject approveTransaction(String idTransaction, String tenant)
    {
        ConfigurationProperties configurationProperties = new ConfigurationProperties();
        ApiResponseDecisionTransactionObject objResponse = new ApiResponseDecisionTransactionObject();
        try {
            ApproveTransactionObject objApprove = new ApproveTransactionObject();
            objApprove = (ApproveTransactionObject) configurationProperties.getConfig("approveParameters", objApprove, ApproveTransactionObject.class);
            
            String url = objApprove.getUrl()+ idTransaction + objApprove.getComplement();
            
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set(HeaderParams.TENANT, tenant);
            Gson gson = new Gson();
            
            HttpEntity<String> requestHttp = new HttpEntity<String>("", headers);
            
            ResponseEntity<ApiResponseDecisionTransactionObject> result = restTemplate.exchange(url, HttpMethod.valueOf(objApprove.getMethod()), requestHttp, ApiResponseDecisionTransactionObject.class);
            
            objResponse = result.getBody();
            
            
            return objResponse;
        } catch (Exception e) {
            logger.info("No se ha podido aprobar la transacción -> "+idTransaction);
            e.printStackTrace();
            objResponse.setRc("1");
            objResponse.setMsg("Fallo");
            return objResponse;
        }
    }
    
    @Override
    public ApiResponseDecisionTransactionObject rejectTransaction(String idTransaction,TransactionDesicionObject objTransaction, String tenant)
    {
        ConfigurationProperties configurationProperties = new ConfigurationProperties();
        ApiResponseDecisionTransactionObject objResponse = new ApiResponseDecisionTransactionObject();
        try {
            RejectTransactionObject objReject = new RejectTransactionObject();
            objReject = (RejectTransactionObject) configurationProperties.getConfig("rejectParameters", objReject, RejectTransactionObject.class);
            
            String url = objReject.getUrl();
            
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set(HeaderParams.TENANT, tenant);
            Gson gson = new Gson();
            HttpEntity<String> requestHttp = new HttpEntity<String>(gson.toJson(objTransaction), headers);
            
            ResponseEntity<ApiResponseDecisionTransactionObject> result = restTemplate.exchange(url, HttpMethod.valueOf(objReject.getMethod()), requestHttp, ApiResponseDecisionTransactionObject.class);
            
            objResponse = result.getBody();
            
            
            return objResponse;
        } catch (Exception e) {
            logger.info("No se ha podido rechazar la transacción"+idTransaction);
            e.printStackTrace();
            objResponse.setRc("1");
            objResponse.setMsg("Fallo");
            return objResponse;
        }
    }
}
