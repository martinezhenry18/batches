/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoInternalTransactionStatusBatch.service;

import java.io.IOException;
import java.util.Map;
import org.springframework.stereotype.Service;

/**
 *
 * @author emarmole
 */
public interface NovoInternalNotificationCenterService 
{
    void internalTransactionsNotification(String mensaje, String type) throws IOException, Exception;
    void internalTransactionsNotificationExternal(Map<String,Object> params, String tenant) throws IOException, Exception;
}
