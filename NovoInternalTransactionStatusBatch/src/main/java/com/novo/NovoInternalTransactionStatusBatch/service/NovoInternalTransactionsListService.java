/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoInternalTransactionStatusBatch.service;

import com.novo.NovoInternalTransactionStatusBatch.Objects.TransactionObject;
import java.util.ArrayList;
import org.springframework.stereotype.Service;

/**
 *
 * @author emarmole
 */
public interface NovoInternalTransactionsListService 
{
    public ArrayList<TransactionObject> getListTransactions() throws Exception;
}
