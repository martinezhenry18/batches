/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoInternalTransactionStatusBatch.Objects;

import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author emarmole
 */
public class TransactionDesicionObject {
    
    private String fraudHoldResolutionDate;
    private String transactionRefId;
    private String reason;

    public String getFraudHoldResolutionDate() {
        return fraudHoldResolutionDate;
    }

    public void setFraudHoldResolutionDate(String fraudHoldResolutionDate) {
        this.fraudHoldResolutionDate = fraudHoldResolutionDate;
    }

    public String getTransactionRefId() {
        return transactionRefId;
    }

    public void setTransactionRefId(String transactionRefId) {
        this.transactionRefId = transactionRefId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    
    
    
    
    
}
