/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoInternalTransactionStatusBatch.Objects;

/**
 *
 * @author emarmole
 */
public class MsAlertsObject 
{
    private String noTransactions;
    private String failedNiceAuth;
    private String sendingProcess;
    private String failedGetTransactions;
    private String finalProcess;

    public String getNoTransactions() {
        return noTransactions;
    }

    public void setNoTransactions(String noTransactions) {
        this.noTransactions = noTransactions;
    }

    public String getFailedNiceAuth() {
        return failedNiceAuth;
    }

    public void setFailedNiceAuth(String failedNiceAuth) {
        this.failedNiceAuth = failedNiceAuth;
    }

    public String getSendingProcess() {
        return sendingProcess;
    }

    public void setSendingProcess(String sendingProcess) {
        this.sendingProcess = sendingProcess;
    }

    public String getFailedGetTransactions() {
        return failedGetTransactions;
    }

    public void setFailedGetTransactions(String failedGetTransactions) {
        this.failedGetTransactions = failedGetTransactions;
    }

    public String getFinalProcess() {
        return finalProcess;
    }

    public void setFinalProcess(String finalProcess) {
        this.finalProcess = finalProcess;
    }
    
    
}
