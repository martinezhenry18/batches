/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoInternalTransactionStatusBatch.Objects;

/**
 *
 * @author emarmole
 */
public class GeneralConfigurationDecision {
    private String reviewedByTransaction;
    private String commentsTransaction;
    private String decisionTypeId;

    public String getReviewedByTransaction() {
        return reviewedByTransaction;
    }

    public void setReviewedByTransaction(String reviewedByTransaction) {
        this.reviewedByTransaction = reviewedByTransaction;
    }

    public String getCommentsTransaction() {
        return commentsTransaction;
    }

    public void setCommentsTransaction(String commentsTransaction) {
        this.commentsTransaction = commentsTransaction;
    }

    public String getDecisionTypeId() {
        return decisionTypeId;
    }

    public void setDecisionTypeId(String decisionTypeId) {
        this.decisionTypeId = decisionTypeId;
    }
    
    
}
