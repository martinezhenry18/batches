/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoInternalTransactionStatusBatch.Objects;

import java.util.List;

/**
 *
 * @author emarmole
 */
public class ExternalTransactionsObject {
    private String customerID;
    private List<TransactionObject> transactions;
    private String version;

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<TransactionObject> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<TransactionObject> transactions) {
        this.transactions = transactions;
    }
   
    
}
