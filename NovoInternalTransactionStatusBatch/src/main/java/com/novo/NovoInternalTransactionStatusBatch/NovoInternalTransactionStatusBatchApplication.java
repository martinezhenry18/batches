package com.novo.NovoInternalTransactionStatusBatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class NovoInternalTransactionStatusBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(NovoInternalTransactionStatusBatchApplication.class, args);
	}

}
