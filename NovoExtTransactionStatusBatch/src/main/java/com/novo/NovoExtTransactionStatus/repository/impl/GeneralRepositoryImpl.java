/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.repository.impl;

import com.novo.NovoExtTransactionStatus.Objects.DbObject;
import com.novo.NovoExtTransactionStatus.config.ConfigurationProperties;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.pool.OracleConnectionPoolDataSource;
import oracle.jdbc.pool.OracleDataSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author emarmole
 */
@Component
public class GeneralRepositoryImpl 
{
    
    public OracleDataSource getConnection() throws SQLException, Exception
    {
        OracleDataSource dataSource = null;
        ConfigurationProperties con = new ConfigurationProperties();
        DbObject objDatabase = new DbObject();
        try {
            dataSource = new OracleConnectionPoolDataSource();
        
            objDatabase = (DbObject) con.getConfig("dbConnection", objDatabase, DbObject.class);
            
            dataSource.setDatabaseName(objDatabase.getDatabase());
            String conectionString = "jdbc:oracle:thin:@"+objDatabase.getIp()+":"+objDatabase.getPort()+":"+objDatabase.getSid();
            dataSource.setURL(conectionString);
            dataSource.setUser(objDatabase.getUser());
            dataSource.setPassword(objDatabase.getPassword());

            return dataSource;
        } catch (Exception ex) {
            Logger.getLogger(GeneralRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } 
    }
    
}
