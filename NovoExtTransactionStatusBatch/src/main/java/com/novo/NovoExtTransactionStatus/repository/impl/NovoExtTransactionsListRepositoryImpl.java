/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.repository.impl;

import com.novo.NovoExtTransactionStatus.Objects.GeneralProcedureDbObject;
import com.novo.NovoExtTransactionStatus.Objects.MsAlertsObject;
import com.novo.NovoExtTransactionStatus.Objects.ProcedureDbObject;
import com.novo.NovoExtTransactionStatus.Objects.TransactionObject;
import com.novo.NovoExtTransactionStatus.config.ConfigurationProperties;
import com.novo.NovoExtTransactionStatus.repository.NovoExtTransactionsListRepository;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

/**
 *
 * @author emarmole
 */
@Repository
public class NovoExtTransactionsListRepositoryImpl extends GeneralRepositoryImpl implements NovoExtTransactionsListRepository
{
    Logger logger = Logger.getLogger(NovoExtTransactionsListRepositoryImpl.class);
    
    @Autowired
    private ConfigurationProperties configProps;
    
    @Override
    public ArrayList<TransactionObject> getListTransactions() throws Exception
    {
        DOMConfigurator.configure(System.getProperty("user.dir")+"/config/log4j.xml");
        OracleDataSource dataSource = getConnection();
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        GeneralProcedureDbObject obj = new GeneralProcedureDbObject();
        MsAlertsObject objMsAlertsObject = new MsAlertsObject();
        ProcedureDbObject objProcedureDbObject;
        obj = (GeneralProcedureDbObject) configProps.getConfig("dataProcedureTransaction", obj, GeneralProcedureDbObject.class);
        objMsAlertsObject = (MsAlertsObject) configProps.getConfig("msAlertMessages", objMsAlertsObject, MsAlertsObject.class);
        objProcedureDbObject = obj.getListTransactionsEscalate();
        
        String sql = objProcedureDbObject.getDbProcedure();
        String pkg = objProcedureDbObject.getDbPackage();
        ArrayList<TransactionObject> listCustomers = new ArrayList<>();
        try {
            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withCatalogName(pkg).withProcedureName(sql)
                    .returningResultSet("TRANSACTIONS", new CostumerMapperUtil());
            Map<String, Object> out = simpleJdbcCall.execute();       
            listCustomers = (ArrayList<TransactionObject>) out.get("TRANSACTIONS");
            return listCustomers;
        } catch (Exception e) {
            //riskNotificationCenterService.riskNotification(objMsAlertsObject.getFailedGetCustomers(), "error");
            e.printStackTrace();
            return listCustomers;
        }
    }
    
    private static class CostumerMapperUtil implements RowMapper<TransactionObject>
    {
        @Override
        public TransactionObject mapRow(ResultSet rs, int i) throws SQLException {
            return new TransactionObject(rs.getString("transactionId"), rs.getString("transferType"));
        }        
    }
}
