package com.novo.NovoExtTransactionStatus.controller;

import com.novo.NovoExtTransactionStatus.Objects.ApiResponseAutenticateObject;
import com.novo.NovoExtTransactionStatus.Objects.ApiResponseDecisionTransactionObject;
import com.novo.NovoExtTransactionStatus.Objects.BodyNotificationObject;
import com.novo.NovoExtTransactionStatus.Objects.DecisionTypeObject;
import com.novo.NovoExtTransactionStatus.Objects.ExternalTransactionsObject;
import com.novo.NovoExtTransactionStatus.Objects.GeneralConfigurationDecision;
import com.novo.NovoExtTransactionStatus.Objects.GeneralParametersObject;
import com.novo.NovoExtTransactionStatus.Objects.MsAlertsObject;
import com.novo.NovoExtTransactionStatus.Objects.ReportContExtTransactionObject;
import com.novo.NovoExtTransactionStatus.Objects.TransactionDesicionObject;
import com.novo.NovoExtTransactionStatus.Objects.TransactionObject;
import com.novo.NovoExtTransactionStatus.Objects.TransactionResponseObject;
import com.novo.NovoExtTransactionStatus.components.ListComponentsComponent;
import com.novo.NovoExtTransactionStatus.config.ConfigurationProperties;
import com.novo.NovoExtTransactionStatus.service.NovoExtDecisionTransactionService;
import com.novo.NovoExtTransactionStatus.service.NovoExtNiceConsumeService;
import com.novo.NovoExtTransactionStatus.service.NovoExtNotificationCenterService;
import com.novo.NovoExtTransactionStatus.service.NovoExtTransactionsListService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author emarmole
 */
@Component
public class ExtTransactionsStatusController 
{
    private static Logger logger = Logger.getLogger(ExtTransactionsStatusController.class);
    
    @Autowired
    private NovoExtTransactionsListService novoExtTransactionsListService;
    
    
    private ListComponentsComponent listComponentsComponent;
    
    @Autowired
    private NovoExtNotificationCenterService novoExtNotificationCenterService;
    
    @Autowired
    private NovoExtNiceConsumeService novoExtNiceConsumeService;
    
    @Autowired
    private NovoExtDecisionTransactionService novoExtDecisionTransactionService;
    
    
    @Bean
    public void extTransactionStatus() throws Exception
    {
        DOMConfigurator.configure(System.getProperty("user.dir")+"/config/log4j.xml");
        ConfigurationProperties config = new ConfigurationProperties();
        Map<String, String> transactionStatus = new HashMap<String, String>();
        transactionStatus = (Map<String, String>) config.getConfig("transactionStatusNice", transactionStatus, HashMap.class);
        
        Map<String, String> defineStatus = new HashMap<String, String>();
        defineStatus = (Map<String, String>) config.getConfig("defineStstuaTransactions", defineStatus, HashMap.class);
        
        
        ReportContExtTransactionObject objReport = new ReportContExtTransactionObject();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        GeneralParametersObject objGeneralParameters = new GeneralParametersObject();
        GeneralConfigurationDecision objGeneralConfiguration = new GeneralConfigurationDecision();
        MsAlertsObject objMsAlerts = new MsAlertsObject();        
        ApiResponseDecisionTransactionObject objResponseDecision;
        TransactionDesicionObject objTransactionDecisionObject;
        ApiResponseAutenticateObject objResponseAutenticate;
        objMsAlerts = (MsAlertsObject) config.getConfig("msAlertMessages", objMsAlerts, MsAlertsObject.class);
        String systemId = "";
        
        novoExtNotificationCenterService.extTransactionsNotification("EXTERNAL TRANSACTION FRAUD PROCESS INIT", "info");
        
        objGeneralParameters = (GeneralParametersObject) config.getConfig("generalParameters", objGeneralParameters, GeneralParametersObject.class);
        objGeneralConfiguration = (GeneralConfigurationDecision) config.getConfig("generalConfigurationDecision", objGeneralConfiguration, GeneralConfigurationDecision.class);
        
        systemId = objGeneralParameters.getSystemId();
        
        ArrayList<TransactionObject> listTransactions = novoExtTransactionsListService.getListTransactions();
        
                
        if (listTransactions.size() > 0) 
        {
            objReport.setTotalTransactions(listTransactions.size());
            if (!systemId.isEmpty()) 
            {
                listTransactions = listComponentsComponent.addSystemIdToTransactions(listTransactions, systemId);
            }            
            try {
                objResponseAutenticate = novoExtNiceConsumeService.consumeWebRestService();
                
                if (objResponseAutenticate.isLogin()) 
                {
                    ExternalTransactionsObject objExternalTransactionsObject = new ExternalTransactionsObject();
                    objExternalTransactionsObject.setCustomerID(objGeneralParameters.getCustomerId());
                    objExternalTransactionsObject.setVersion(objGeneralParameters.getVersion());
                    objExternalTransactionsObject.setTransactions(listTransactions);
                    ArrayList<TransactionResponseObject> listResponse = novoExtNiceConsumeService.consumeTransactionApiFraud(objResponseAutenticate, objExternalTransactionsObject);
                    
                    for (TransactionResponseObject transactionResponseObject : listResponse) 
                    {
                        String status = transactionStatus.get(transactionResponseObject.getResult());
                        int idStatus = Integer.parseInt(defineStatus.get(status));
                        String idTransaction = transactionResponseObject.getTransactionID().split("_")[1];
                        
                        switch (idStatus)
                        {
                            case 1:
                                objReport.setHoldTransactions(objReport.getHoldTransactions() + 1);
                                break;
                            case 2:
                                objResponseDecision = new ApiResponseDecisionTransactionObject();
                                
                                DecisionTypeObject objDecision = new DecisionTypeObject();
//                                
                                objDecision.setId(objGeneralConfiguration.getDecisionTypeId());
//                                
//                                ArrayList<DecisionTypeObject> listIdDecision = new ArrayList<DecisionTypeObject>();
//                                listIdDecision.add(objDecision);
                                
                                Map<String, String> listIdDecision = new HashMap<String, String>();
                                listIdDecision.put("id", objDecision.getId());
                                
                                
                                
                                objTransactionDecisionObject = new TransactionDesicionObject();
                                objTransactionDecisionObject.setFraudHoldResolutionDate(status);
                                objTransactionDecisionObject.setComments("REJECT TRANSACTION FROM NICE ACTIMIZE");
                                objTransactionDecisionObject.setReviewedBy(objGeneralConfiguration.getReviewedByTransaction());
                                objTransactionDecisionObject.setFraudHoldResolutionDate(format.format(new Date()));
                                objTransactionDecisionObject.setDecisionType(listIdDecision);
                                
                                
                                objResponseDecision = novoExtDecisionTransactionService.rejectTransaction(idTransaction, objTransactionDecisionObject, objGeneralParameters.getTenant());
                                
                                if (!objResponseDecision.getRc().equals("0")) 
                                {
                                    objReport.setFailedApproveTransactions(objReport.getFailedCancelTransactions() + 1);
                                    novoExtNotificationCenterService.extTransactionsNotification("Fallo el rechazo de la transacción #"+idTransaction, "error");
                                } else {
                                    objReport.setCancelTransactions(objReport.getCancelTransactions() + 1);
                                }
                                break;
                            case 3:
                                objResponseDecision = new ApiResponseDecisionTransactionObject();
                                
                                objTransactionDecisionObject = new TransactionDesicionObject();
                                objTransactionDecisionObject.setFraudHoldResolutionDate(status);
                                objTransactionDecisionObject.setReviewedBy(objGeneralConfiguration.getReviewedByTransaction());
                                objTransactionDecisionObject.setComments("APPROVE TRANSACTION FROM NICE ACTIMIZE");
                                objTransactionDecisionObject.setFraudHoldResolutionDate(format.format(new Date()));
                                
                                objResponseDecision = novoExtDecisionTransactionService.approveTransaction(idTransaction, objTransactionDecisionObject, objGeneralParameters.getTenant());
                                
                                if (!objResponseDecision.getRc().equals("0")) 
                                {
                                    objReport.setFailedApproveTransactions(objReport.getFailedApproveTransactions() + 1);
                                    novoExtNotificationCenterService.extTransactionsNotification("Fallo la aprobación de la transacción #"+idTransaction, "error");
                                } else {
                                    objReport.setReleaseTransactions(objReport.getReleaseTransactions() + 1);
                                }
                                
                                break;
                            case 4:
                                objReport.setUnknownTransactions(objReport.getUnknownTransactions() + 1);
                                break;
                        }
                    }
                    Map<String, Object> params = new HashMap<>();
                    BodyNotificationObject objBody = new BodyNotificationObject(listTransactions, listResponse);
                    params.put("TRANSACTIONS_LIST", objBody.getTransactionList());
                    novoExtNotificationCenterService.extTransactionsNotificationExternal(params, objGeneralParameters.getTenant());
                } else {
                    novoExtNotificationCenterService.extTransactionsNotification(objMsAlerts.getFailedNiceAuth(), "error");
                }
            } catch (Exception e) {
                e.printStackTrace();
                novoExtNotificationCenterService.extTransactionsNotification(objMsAlerts.getFailedGetTransactions(), "error");
            }                        
        } else {
            novoExtNotificationCenterService.extTransactionsNotification(objMsAlerts.getNoTransactions(), "info");
        }
        String textFinal = "";
        textFinal  = "<ul><li><b>Summary:</b></li>";
        textFinal += "<ul><li>Total Transactions: "+Integer.toString(objReport.getTotalTransactions())+"</li>";
        textFinal += "<li>Hold: "+Integer.toString(objReport.getHoldTransactions())+"</li>";
        textFinal += "<li>Cancel: "+Integer.toString(objReport.getCancelTransactions())+"</li>";
        textFinal += "<li>Approve: "+Integer.toString(objReport.getReleaseTransactions())+"</li>";
        textFinal += "<li>Failed Approve: "+Integer.toString(objReport.getFailedApproveTransactions())+"</li>";
        textFinal += "<li>Failed Cancel: "+Integer.toString(objReport.getFailedCancelTransactions())+"</li></ul></ul>";
        novoExtNotificationCenterService.extTransactionsNotification(textFinal, "success");
    }
}
