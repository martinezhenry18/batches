/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.Objects;

import com.novo.NovoExtTransactionStatus.constant.TransactionPrefix;

/**
 *
 * @author emarmole
 */
public class TransactionObject {
    private String transactionID;
    private String transactionOriginalID;
    private String transferType;
    private String systemId;

    public TransactionObject(String transactionID, String transferType) {
        this.transactionID = TransactionPrefix.getPrefixByType(transferType) + "_" +transactionID;
        this.transferType = transferType;
        this.transactionOriginalID = transactionID;
    }    
    
    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    public String getTransactionOriginalID() {
        return transactionOriginalID;
    }

    public void setTransactionOriginalID(String transactionOriginalID) {
        this.transactionOriginalID = transactionOriginalID;
    }
}
