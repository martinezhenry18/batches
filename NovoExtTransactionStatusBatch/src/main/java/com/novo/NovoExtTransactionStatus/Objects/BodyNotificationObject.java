/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.Objects;

import java.util.ArrayList;

/**
 *
 * @author emarmole
 */
public class BodyNotificationObject 
{
    private String transactionList;

    public BodyNotificationObject(ArrayList<TransactionObject> listTransactions, ArrayList<TransactionResponseObject> listResponse) 
    {
        this.transactionList = "";
        for (TransactionObject transactionObject : listTransactions) {
            this.transactionList += "<tr>";
            this.transactionList += "<td>"+transactionObject.getTransactionOriginalID()+"</td>";
            this.transactionList += "<td>"+transactionObject.getTransactionID()+"</td>";
            this.transactionList += "<td>"+transactionObject.getTransferType().toUpperCase()+"</td>";
            for (TransactionResponseObject transactionResponseObject : listResponse) 
            {
                if (transactionResponseObject.getTransactionID().equals(transactionObject.getTransactionID())) 
                {
                    this.transactionList += "<td>"+transactionResponseObject.getResult()+"</td>";
                }
            }
            this.transactionList += "</tr>";
        }
    }

    public String getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(String transactionList) {
        this.transactionList = transactionList;
    }
    
    
}
