/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.Objects;

/**
 *
 * @author emarmole
 */
public class ApiTransactionFraudObject 
{
    private String url;
    private String complUrl;
    private String method;
    private boolean requiredToken;
    private String token;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getComplUrl() {
        return complUrl;
    }

    public void setComplUrl(String complUrl) {
        this.complUrl = complUrl;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public boolean isRequiredToken() {
        return requiredToken;
    }

    public void setRequiredToken(boolean requiredToken) {
        this.requiredToken = requiredToken;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
