/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.Objects;

import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author emarmole
 */
public class NotificationConnectorContextObject 
{
    private String to;
    private String body;
    private String subject;
    private String channel;
    private String appIdentifier;
    private ArrayList<String> cc;
    private Map<String, Object> variableData;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Map<String, Object> getVariableData() {
        return variableData;
    }

    public void setVariableData(Map<String, Object> variableData) {
        this.variableData = variableData;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getAppIdentifier() {
        return appIdentifier;
    }

    public void setAppIdentifier(String appIdentifier) {
        this.appIdentifier = appIdentifier;
    }

    public ArrayList<String> getCc() {
        return cc;
    }

    public void setCc(ArrayList<String> cc) {
        this.cc = cc;
    }
    
}
