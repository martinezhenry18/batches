/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.Objects;

import java.util.ArrayList;

/**
 *
 * @author emarmole
 */
public class NotificationConnectorObject 
{
    private boolean active;
    private String emailNotification;
    private String templateNotification;
    private String urlNotification;
    private String proveedorNotification;
    private String method;
    private String subject;
    private String channel;
    private String appIdentifier;
    private String countrySend;
    private ArrayList<String> cc;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
    public String getEmailNotification() {
        return emailNotification;
    }

    public void setEmailNotification(String emailNotification) {
        this.emailNotification = emailNotification;
    }

    public String getTemplateNotification() {
        return templateNotification;
    }

    public void setTemplateNotification(String templateNotification) {
        this.templateNotification = templateNotification;
    }

    public String getUrlNotification() {
        return urlNotification;
    }

    public void setUrlNotification(String urlNotification) {
        this.urlNotification = urlNotification;
    }

    public String getProveedorNotification() {
        return proveedorNotification;
    }

    public void setProveedorNotification(String proveedorNotification) {
        this.proveedorNotification = proveedorNotification;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getAppIdentifier() {
        return appIdentifier;
    }

    public void setAppIdentifier(String appIdentifier) {
        this.appIdentifier = appIdentifier;
    }

    public String getCountrySend() {
        return countrySend;
    }

    public void setCountrySend(String countrySend) {
        this.countrySend = countrySend;
    }

    public ArrayList<String> getCc() {
        return cc;
    }

    public void setCc(ArrayList<String> cc) {
        this.cc = cc;
    }
    
    
}
