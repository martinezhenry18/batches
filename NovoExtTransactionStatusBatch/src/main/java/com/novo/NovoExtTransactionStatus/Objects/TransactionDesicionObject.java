/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.Objects;

import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author emarmole
 */
public class TransactionDesicionObject {
    
    private String fraudHoldResolutionDate;
    private String reviewedBy;
    private String comments;
    private Map<String, String> decisionType;

    public String getFraudHoldResolutionDate() {
        return fraudHoldResolutionDate;
    }

    public void setFraudHoldResolutionDate(String fraudHoldResolutionDate) {
        this.fraudHoldResolutionDate = fraudHoldResolutionDate;
    }

    public String getReviewedBy() {
        return reviewedBy;
    }

    public void setReviewedBy(String reviewedBy) {
        this.reviewedBy = reviewedBy;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Map<String, String> getDecisionType() {
        return decisionType;
    }

    public void setDecisionType(Map<String, String> decisionType) {
        this.decisionType = decisionType;
    }
    
    
    
    
}
