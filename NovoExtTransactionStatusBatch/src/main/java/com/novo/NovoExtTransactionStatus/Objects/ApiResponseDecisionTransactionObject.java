/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.Objects;

/**
 *
 * @author emarmole
 */
public class ApiResponseDecisionTransactionObject 
{
    private String rc;
    private String msg;

    public String getRc() {
        return rc;
    }

    public void setRc(String rc) {
        this.rc = rc;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    
    
}
