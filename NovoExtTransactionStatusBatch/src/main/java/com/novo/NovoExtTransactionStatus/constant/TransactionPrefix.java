/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.constant;

/**
 *
 * @author emarmole
 */
public enum TransactionPrefix {
    EXTERNAL_OUT_ACH("ACH","EOA"),
    EXTERNAL_OUT_WIRE("WIRE","EOW");
    
    private String type;
    private String prefix;

    private TransactionPrefix(String type, String prefix) {
        this.type = type;
        this.prefix = prefix;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
        
    public static String getPrefixByType(String type) {
        for (TransactionPrefix e : values()) {
            if (e.type.equals(type)) {
                return e.prefix;
            }
        }
        return "WIRE";
    }
    
}
