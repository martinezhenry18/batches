/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.constant;

/**
 *
 * @author emarmole
 */
public interface HeaderParams {
    String TENANT = "x-context-id";
    String X_AMZ_SECURITY = "X-Amz-Security-Token";
    String Content_type = "Content-type";
}
