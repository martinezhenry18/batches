/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.service;

import com.novo.NovoExtTransactionStatus.Objects.ApiResponseAutenticateObject;
import com.novo.NovoExtTransactionStatus.Objects.ExternalTransactionsObject;
import com.novo.NovoExtTransactionStatus.Objects.TransactionResponseObject;
import java.io.IOException;
import java.util.ArrayList;
import org.springframework.stereotype.Service;

/**
 *
 * @author emarmole
 */
public interface NovoExtNiceConsumeService 
{
    public ApiResponseAutenticateObject consumeWebRestService() throws IOException, Exception;
    
    public ArrayList<TransactionResponseObject> consumeTransactionApiFraud(ApiResponseAutenticateObject objAuth, ExternalTransactionsObject objExternalTransactions) throws IOException, Exception;
}
