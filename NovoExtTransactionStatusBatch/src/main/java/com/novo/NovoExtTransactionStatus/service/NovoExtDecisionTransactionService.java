/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.service;

import com.novo.NovoExtTransactionStatus.Objects.ApiResponseDecisionTransactionObject;
import com.novo.NovoExtTransactionStatus.Objects.TransactionDesicionObject;

/**
 *
 * @author emarmole
 */
public interface NovoExtDecisionTransactionService 
{
    public ApiResponseDecisionTransactionObject approveTransaction(String idTransaction,TransactionDesicionObject objTransaction, String tenant);
    public ApiResponseDecisionTransactionObject rejectTransaction(String idTransaction,TransactionDesicionObject objTransaction, String tenant);
}
