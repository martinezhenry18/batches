/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.service.impl;

import com.novo.NovoExtTransactionStatus.Objects.TransactionObject;
import com.novo.NovoExtTransactionStatus.repository.NovoExtTransactionsListRepository;
import com.novo.NovoExtTransactionStatus.service.NovoExtTransactionsListService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author emarmole
 */
@Service
public class NovoExtTransactionsListServiceImpl implements NovoExtTransactionsListService
{

    @Autowired
    private NovoExtTransactionsListRepository novoExtTransactionsListRepository;
    
    @Override
    public ArrayList<TransactionObject> getListTransactions() throws Exception {
        return novoExtTransactionsListRepository.getListTransactions();
    }
    
}
