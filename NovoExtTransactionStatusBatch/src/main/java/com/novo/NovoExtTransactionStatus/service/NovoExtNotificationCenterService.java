/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.service;

import java.io.IOException;
import java.util.Map;
import org.springframework.stereotype.Service;

/**
 *
 * @author emarmole
 */
public interface NovoExtNotificationCenterService 
{
    void extTransactionsNotification(String mensaje, String type) throws IOException, Exception;
    void extTransactionsNotificationExternal(Map<String,Object> params, String tenant) throws IOException, Exception;
}
