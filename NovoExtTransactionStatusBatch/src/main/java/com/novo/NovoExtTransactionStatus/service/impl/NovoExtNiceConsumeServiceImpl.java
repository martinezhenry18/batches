/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.service.impl;

import com.amazonaws.auth.AWS4Signer;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.google.gson.Gson;
import com.novo.NovoExtTransactionStatus.Objects.ApiAutenticateObject;
import com.novo.NovoExtTransactionStatus.Objects.ApiResponseAutenticateObject;
import com.novo.NovoExtTransactionStatus.Objects.ApiTransactionFraudObject;
import com.novo.NovoExtTransactionStatus.Objects.ExternalTransactionsObject;
import com.novo.NovoExtTransactionStatus.Objects.MsAlertsObject;
import com.novo.NovoExtTransactionStatus.Objects.NiceAwsParametersObject;
import com.novo.NovoExtTransactionStatus.Objects.TransactionResponseObject;
import com.novo.NovoExtTransactionStatus.aws.components.AWSRequestSigningApacheInterceptor;
import com.novo.NovoExtTransactionStatus.config.ConfigurationProperties;
import com.novo.NovoExtTransactionStatus.constant.HeaderParams;
import com.novo.NovoExtTransactionStatus.service.NovoExtNiceConsumeService;
import com.novo.NovoExtTransactionStatus.service.NovoExtNotificationCenterService;
import org.apache.commons.io.IOUtils;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author emarmole
 */
@Service
public class NovoExtNiceConsumeServiceImpl extends AWSCredentialsProviderImpl implements NovoExtNiceConsumeService
{
    
    @Autowired
    NovoExtNotificationCenterService novoExtNotificationCenterService;
    

    @Override
    public ApiResponseAutenticateObject consumeWebRestService() throws IOException, Exception {
        ConfigurationProperties configurationProperties = new ConfigurationProperties();
        ApiResponseAutenticateObject objApiResponse = new ApiResponseAutenticateObject();
        try {
            
            ApiAutenticateObject objApiAutenticateObject = new ApiAutenticateObject();
            MsAlertsObject objMsAlertsObject = new MsAlertsObject();
            objApiAutenticateObject = (ApiAutenticateObject) configurationProperties.getConfig("autenticateApiFraud", objApiAutenticateObject, ApiAutenticateObject.class);
            objMsAlertsObject = (MsAlertsObject) configurationProperties.getConfig("msAlertMessages", objMsAlertsObject, MsAlertsObject.class);

            RestTemplate rest = new RestTemplate();
            Gson gson = new Gson();
            HttpHeaders header = new HttpHeaders();
            
            MediaType mediaType = new MediaType("application", "json", StandardCharsets.UTF_8);
            header.setContentType(mediaType);
            
            HttpEntity<String> requestHttpEntity = new HttpEntity<String>(gson.toJson(objApiAutenticateObject), header);
            
            String result = rest.postForObject(objApiAutenticateObject.getRuta(),requestHttpEntity, String.class);
            
            objApiResponse = gson.fromJson(result, ApiResponseAutenticateObject.class);
                    
            if (!objApiResponse.isLogin()) 
            {
                String failedError = objMsAlertsObject.getFailedNiceAuth();
                novoExtNotificationCenterService.extTransactionsNotification(failedError, "error");
            } else {
                String successLogin = "Successfully authenticated in Nice Actimize services";
                novoExtNotificationCenterService.extTransactionsNotification(successLogin, "info");
            }
            
            return objApiResponse;
            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw e;
        }
    }
    
    @Override
    public ArrayList<TransactionResponseObject> consumeTransactionApiFraud(ApiResponseAutenticateObject objAuth, ExternalTransactionsObject objExternalTransactions) throws IOException, Exception 
    {
        ConfigurationProperties config = new ConfigurationProperties();
        ApiTransactionFraudObject objFraudTransaction = new ApiTransactionFraudObject();
        NiceAwsParametersObject objNice = new NiceAwsParametersObject();
        
        objNice = (NiceAwsParametersObject) config.getConfig("niceAWSParameters", objNice, NiceAwsParametersObject.class);
        objFraudTransaction = (ApiTransactionFraudObject) config.getConfig("transactionFraudApi", objFraudTransaction, ApiTransactionFraudObject.class);
        Gson gson = new Gson();
        
        HttpClientBuilder builder = HttpClientBuilder.create();
        
        AWS4Signer signer = new AWS4Signer();
        signer.setRegionName(objNice.getRegionName());
        signer.setServiceName(objNice.getServiceName());
        
        AWSCredentialsProvider credentials = new AWSCredentialsProviderImpl(objAuth.getAccessKeyId(), objAuth.getSecretAccessKey());
        
        AWSRequestSigningApacheInterceptor requestInt = new AWSRequestSigningApacheInterceptor(objNice.getServiceName(), signer, credentials);
        
        builder.addInterceptorLast(requestInt);
        
        try (CloseableHttpClient client = builder.build())
        {
            String url = objFraudTransaction.getUrl() + objNice.getCustomerId() + objFraudTransaction.getComplUrl();
            HttpPost post = new HttpPost(url);
            post.addHeader(HeaderParams.X_AMZ_SECURITY, objAuth.getSessionToken());
            String body = gson.toJson(objExternalTransactions);
            StringEntity bodyEntity = new StringEntity(body);
            bodyEntity.setContentType("application/json; charset=utf-8");
            post.setEntity(bodyEntity);
            post.setHeader(HeaderParams.Content_type,"application/json; charset=utf-8");
            CloseableHttpResponse response = client.execute(post);
            
            String responseStr = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8.name());
            Map<String, Object> res = gson.fromJson(responseStr, HashMap.class);
            ArrayList<Map<String,String>> listTransactions = gson.fromJson(gson.toJson(res.get("transactions")), ArrayList.class);
            ArrayList<TransactionResponseObject> responseListTransactions = new ArrayList<>();
            for (Map<String, String> obj : listTransactions) {
                TransactionResponseObject objRes = new TransactionResponseObject();
                objRes.setReasonCode(obj.get("reasonCode"));
                objRes.setResult(obj.get("result"));
                objRes.setSystemID(obj.get("systemID"));
                objRes.setTransactionID(obj.get("transactionID"));
                objRes.setUserName(obj.get("userName"));
                
                responseListTransactions.add(objRes);
            }
            
            return responseListTransactions;
            
        } catch (Exception e) {
            throw e;
        }
    }
    
}
