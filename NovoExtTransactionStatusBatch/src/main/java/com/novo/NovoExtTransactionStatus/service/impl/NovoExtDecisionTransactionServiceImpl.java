/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.service.impl;

import com.google.gson.Gson;
import com.novo.NovoExtTransactionStatus.Objects.ApiResponseDecisionTransactionObject;
import com.novo.NovoExtTransactionStatus.Objects.GeneralParametersObject;
import com.novo.NovoExtTransactionStatus.Objects.TransactionDesicionObject;
import com.novo.NovoExtTransactionStatus.config.ConfigurationProperties;
import com.novo.NovoExtTransactionStatus.constant.HeaderParams;
import com.novo.NovoExtTransactionStatus.service.NovoExtDecisionTransactionService;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author emarmole
 */
@Service
public class NovoExtDecisionTransactionServiceImpl implements NovoExtDecisionTransactionService
{
    private static Logger logger = Logger.getLogger(NovoExtDecisionTransactionServiceImpl.class);
    
    @Override
    public ApiResponseDecisionTransactionObject approveTransaction(String idTransaction,TransactionDesicionObject objTransaction, String tenant)
    {
        DOMConfigurator.configure(System.getProperty("user.dir")+"/config/log4j.xml");
        ConfigurationProperties configurationProperties = new ConfigurationProperties();
        ApiResponseDecisionTransactionObject objResponse = new ApiResponseDecisionTransactionObject();
        try {
            GeneralParametersObject objGeneralParameters = new GeneralParametersObject();
            objGeneralParameters = (GeneralParametersObject) configurationProperties.getConfig("generalParameters", objGeneralParameters, GeneralParametersObject.class);
            
            String url = objGeneralParameters.getGeneralUrl() + idTransaction + objGeneralParameters.getApproveUrl();
            
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set(HeaderParams.TENANT, tenant);
            Gson gson = new Gson();
            
            HttpEntity<String> requestHttp = new HttpEntity<String>(gson.toJson(objTransaction), headers);
            
            //String result = restTemplate.postForObject(url, requestHttp, String.class);
            ResponseEntity<ApiResponseDecisionTransactionObject> result = restTemplate.exchange(url, HttpMethod.PUT, requestHttp, ApiResponseDecisionTransactionObject.class);
            
            objResponse = result.getBody();
            
            
            return objResponse;
        } catch (Exception e) {
            logger.info("No se ha podido aprobar la transacción -> "+idTransaction);
            e.printStackTrace();
            objResponse.setRc("1");
            objResponse.setMsg("Fallo");
            return objResponse;
        }
    }
    
    @Override
    public ApiResponseDecisionTransactionObject rejectTransaction(String idTransaction,TransactionDesicionObject objTransaction, String tenant)
    {
        DOMConfigurator.configure(System.getProperty("user.dir")+"/config/log4j.xml");
        ConfigurationProperties configurationProperties = new ConfigurationProperties();
        ApiResponseDecisionTransactionObject objResponse = new ApiResponseDecisionTransactionObject();
        try {
            GeneralParametersObject objGeneralParameters = new GeneralParametersObject();
            objGeneralParameters = (GeneralParametersObject) configurationProperties.getConfig("generalParameters", objGeneralParameters, GeneralParametersObject.class);
            
            String url = objGeneralParameters.getGeneralUrl() + idTransaction + objGeneralParameters.getRejectUrl();
            
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set(HeaderParams.TENANT, tenant);
            Gson gson = new Gson();
            HttpEntity<String> requestHttp = new HttpEntity<String>(gson.toJson(objTransaction), headers);
            
            //String result = restTemplate.postForObject(url, requestHttp, String.class);
            ResponseEntity<ApiResponseDecisionTransactionObject> result = restTemplate.exchange(url, HttpMethod.PUT, requestHttp, ApiResponseDecisionTransactionObject.class);
            
            objResponse = result.getBody();
            
            
            return objResponse;
        } catch (Exception e) {
            logger.info("No se ha podido rechazar la transacción"+idTransaction);
            e.printStackTrace();
            objResponse.setRc("1");
            objResponse.setMsg("Fallo");
            return objResponse;
        }
    }
}
