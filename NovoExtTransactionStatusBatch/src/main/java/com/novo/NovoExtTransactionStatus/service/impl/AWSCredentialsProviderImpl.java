/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoExtTransactionStatus.service.impl;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import org.springframework.stereotype.Service;

/**
 *
 * @author emarmole
 */
@Service
public class AWSCredentialsProviderImpl implements AWSCredentials, AWSCredentialsProvider {
    private String awsAccessKeyId;
    private String awsSecretKey;

    public AWSCredentialsProviderImpl() {
    }

    public AWSCredentialsProviderImpl(String awsAccessKeyId, String awsSecretKey) {
        this.awsAccessKeyId = awsAccessKeyId;
        this.awsSecretKey = awsSecretKey;
    }
    
    @Override
    public String getAWSAccessKeyId() {
        return this.awsAccessKeyId;
    }

    @Override
    public String getAWSSecretKey() {
        return this.awsSecretKey;
    }

    public void setAwsAccessKeyId(String awsAccessKeyId) {
        this.awsAccessKeyId = awsAccessKeyId;
    }

    public void setAwsSecretKey(String awsSecretKey) {
        this.awsSecretKey = awsSecretKey;
    }

    @Override
    public AWSCredentials getCredentials() {
        return this;
    }

    @Override
    public void refresh() {
        System.out.println("Calling refresh..");
    }
    
    
}
