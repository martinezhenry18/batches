package com.novo.NovoIncExtTransactionStatus.controller;

import com.novo.NovoIncExtTransactionStatus.Objects.ApiResponseAutenticateObject;
import com.novo.NovoIncExtTransactionStatus.Objects.ApiResponseDecisionTransactionObject;
import com.novo.NovoIncExtTransactionStatus.Objects.BodyNotificationObject;
import com.novo.NovoIncExtTransactionStatus.Objects.ExternalTransactionsObject;
import com.novo.NovoIncExtTransactionStatus.Objects.GeneralConfigurationDecision;
import com.novo.NovoIncExtTransactionStatus.Objects.GeneralParametersObject;
import com.novo.NovoIncExtTransactionStatus.Objects.MsAlertsObject;
import com.novo.NovoIncExtTransactionStatus.Objects.ReportContExtTransactionObject;
import com.novo.NovoIncExtTransactionStatus.Objects.TransactionDesicionObject;
import com.novo.NovoIncExtTransactionStatus.Objects.TransactionObject;
import com.novo.NovoIncExtTransactionStatus.Objects.TransactionResponseObject;
import com.novo.NovoIncExtTransactionStatus.components.ListComponentsComponent;
import com.novo.NovoIncExtTransactionStatus.config.ConfigurationProperties;
import com.novo.NovoIncExtTransactionStatus.service.NovoIncExtDecisionTransactionService;
import com.novo.NovoIncExtTransactionStatus.service.NovoIncExtNiceConsumeService;
import com.novo.NovoIncExtTransactionStatus.service.NovoIncExtNotificationCenterService;
import com.novo.NovoIncExtTransactionStatus.service.NovoIncExtTransactionsListService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author emarmole
 */
@Component
public class IncExtTransactionsStatusController 
{
    private static Logger logger = Logger.getLogger(IncExtTransactionsStatusController.class);
    
    @Autowired
    private NovoIncExtTransactionsListService novoIncExtTransactionsListService;
    
    
    private ListComponentsComponent listComponentsComponent;
    
    @Autowired
    private NovoIncExtNotificationCenterService novoIncExtNotificationCenterService;
    
    @Autowired
    private NovoIncExtNiceConsumeService novoIncExtNiceConsumeService;
    
    @Autowired
    private NovoIncExtDecisionTransactionService novoIncExtDecisionTransactionService;
    
    
    @Bean
    public void extTransactionStatus() throws Exception
    {
        DOMConfigurator.configure(System.getProperty("user.dir")+"/config/log4j.xml");
        ConfigurationProperties config = new ConfigurationProperties();
        Map<String, String> transactionStatus = new HashMap<String, String>();
        transactionStatus = (Map<String, String>) config.getConfig("transactionStatusNice", transactionStatus, HashMap.class);
        
        Map<String, String> defineStatus = new HashMap<String, String>();
        defineStatus = (Map<String, String>) config.getConfig("defineStstuaTransactions", defineStatus, HashMap.class);
        
        
        ReportContExtTransactionObject objReport = new ReportContExtTransactionObject();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        GeneralParametersObject objGeneralParameters = new GeneralParametersObject();
        GeneralConfigurationDecision objGeneralConfiguration = new GeneralConfigurationDecision();
        MsAlertsObject objMsAlerts = new MsAlertsObject();        
        ApiResponseDecisionTransactionObject objResponseDecision;
        TransactionDesicionObject objTransactionDecisionObject;
        ApiResponseAutenticateObject objResponseAutenticate;
        objMsAlerts = (MsAlertsObject) config.getConfig("msAlertMessages", objMsAlerts, MsAlertsObject.class);
        String systemId = "";
        
        novoIncExtNotificationCenterService.incExtTransactionsNotification("EXTERNAL INCOMING TRANSACTION FRAUD PROCESS INIT", "info");
        
        objGeneralParameters = (GeneralParametersObject) config.getConfig("generalParameters", objGeneralParameters, GeneralParametersObject.class);
        objGeneralConfiguration = (GeneralConfigurationDecision) config.getConfig("generalConfigurationDecision", objGeneralConfiguration, GeneralConfigurationDecision.class);
        
        systemId = objGeneralParameters.getSystemId();
        
        ArrayList<TransactionObject> listTransactions = novoIncExtTransactionsListService.getListTransactions();
        
//        String[] transactions = {"305","320","323","306"};
//        
//        for (String el : transactions) 
//        {
//            TransactionObject obj = new TransactionObject(el);
//            listTransactions.add(obj);
//        }
        
        if (listTransactions.size() > 0) 
        {
            objReport.setTotalTransactions(listTransactions.size());
            if (!systemId.isEmpty()) 
            {
                listTransactions = listComponentsComponent.addSystemIdToTransactions(listTransactions, systemId);
            }            
            try {
                objResponseAutenticate = novoIncExtNiceConsumeService.consumeWebRestService();
                
                if (objResponseAutenticate.isLogin()) 
                {
                    ExternalTransactionsObject objExternalTransactionsObject = new ExternalTransactionsObject();
                    objExternalTransactionsObject.setCustomerID(objGeneralParameters.getCustomerId());
                    objExternalTransactionsObject.setVersion(objGeneralParameters.getVersion());
                    objExternalTransactionsObject.setTransactions(listTransactions);
                    ArrayList<TransactionResponseObject> listResponse = novoIncExtNiceConsumeService.consumeTransactionApiFraud(objResponseAutenticate, objExternalTransactionsObject);
                    
                    for (TransactionResponseObject transactionResponseObject : listResponse) 
                    {
                        String status = Optional.ofNullable(transactionStatus.get(transactionResponseObject.getResult())).orElse("Unknown");;
                        int idStatus = Integer.parseInt(defineStatus.get(status));
                        String idTransaction = transactionResponseObject.getTransactionID().split("_")[1];
                        switch (idStatus)
                        {
                            case 1:
                                objReport.setHoldTransactions(objReport.getHoldTransactions() + 1);
                                break;
                            case 2:
                                objResponseDecision = new ApiResponseDecisionTransactionObject();
                                
                                objTransactionDecisionObject = new TransactionDesicionObject();
                                objTransactionDecisionObject.setReason("REJECT TRANSACTION FROM NICE ACTIMIZE");
                                objTransactionDecisionObject.setFraudHoldResolutionDate(format.format(new Date()));
                                objTransactionDecisionObject.setTransactionRefId(idTransaction);
                                
                                
                                objResponseDecision = novoIncExtDecisionTransactionService.rejectTransaction(idTransaction, objTransactionDecisionObject, objGeneralParameters.getTenant());
                                
                                if (!objResponseDecision.getRc().equals("0")) 
                                {
                                    objReport.setFailedApproveTransactions(objReport.getFailedCancelTransactions() + 1);
                                    novoIncExtNotificationCenterService.incExtTransactionsNotification("Fallo el rechazo de la transacción #"+idTransaction, "error");
                                } else {
                                    objReport.setCancelTransactions(objReport.getCancelTransactions() + 1);
                                }
                                break;
                            case 3:
                                objResponseDecision = new ApiResponseDecisionTransactionObject();
                                
                                objTransactionDecisionObject = new TransactionDesicionObject();
                                objTransactionDecisionObject.setReason("APPROVE TRANSACTION FROM NICE ACTIMIZE");
                                objTransactionDecisionObject.setFraudHoldResolutionDate(format.format(new Date()));
                                objTransactionDecisionObject.setTransactionRefId(idTransaction);
                                
                                objResponseDecision = novoIncExtDecisionTransactionService.approveTransaction(idTransaction, objTransactionDecisionObject, objGeneralParameters.getTenant());
                                
                                if (!objResponseDecision.getRc().equals("0")) 
                                {
                                    objReport.setFailedApproveTransactions(objReport.getFailedApproveTransactions() + 1);
                                    novoIncExtNotificationCenterService.incExtTransactionsNotification("Fallo la aprobación de la transacción #"+idTransaction, "error");
                                } else {
                                    objReport.setReleaseTransactions(objReport.getReleaseTransactions() + 1);
                                }
                                
                                break;
                            case 4:
                                objReport.setUnknownTransactions(objReport.getUnknownTransactions() + 1);
                                break;
                        }
                    }
                    
                    
                    Map<String, Object> params = new HashMap<>();
                    BodyNotificationObject objBody = new BodyNotificationObject(listTransactions, listResponse);
                    params.put("TRANSACTIONS_LIST", objBody.getTransactionList());
                    novoIncExtNotificationCenterService.incExtTransactionsNotificationExternal(params, objGeneralParameters.getTenant());
                } else {
                    novoIncExtNotificationCenterService.incExtTransactionsNotification(objMsAlerts.getFailedNiceAuth(), "error");
                }
            } catch (Exception e) {
                e.printStackTrace();
                novoIncExtNotificationCenterService.incExtTransactionsNotification(objMsAlerts.getFailedGetTransactions(), "error");
            }                        
        } else {
            novoIncExtNotificationCenterService.incExtTransactionsNotification(objMsAlerts.getNoTransactions(), "info");
        }
        String textFinal = "";
        textFinal  = "<ul><li><b>Summary:</b></li>";
        textFinal += "<ul><li>Total Transactions: "+Integer.toString(objReport.getTotalTransactions())+"</li>";
        textFinal += "<li>Hold: "+Integer.toString(objReport.getHoldTransactions())+"</li>";
        textFinal += "<li>Cancel: "+Integer.toString(objReport.getCancelTransactions())+"</li>";
        textFinal += "<li>Approve: "+Integer.toString(objReport.getReleaseTransactions())+"</li>";
        textFinal += "<li>Failed Approve: "+Integer.toString(objReport.getFailedApproveTransactions())+"</li>";
        textFinal += "<li>Failed Cancel: "+Integer.toString(objReport.getFailedCancelTransactions())+"</li></ul></ul>";
        novoIncExtNotificationCenterService.incExtTransactionsNotification(textFinal, "success");
    }
}
