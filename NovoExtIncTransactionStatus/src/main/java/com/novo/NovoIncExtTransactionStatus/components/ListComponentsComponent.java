/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoIncExtTransactionStatus.components;

import com.novo.NovoIncExtTransactionStatus.Objects.TransactionObject;
import java.util.ArrayList;

/**
 *
 * @author emarmole
 */
public class ListComponentsComponent 
{
    
    
    public ArrayList<TransactionObject> addSystemIdToTransactions(ArrayList<TransactionObject> lista, String systemId)
    {
        ArrayList<TransactionObject> listTransactions = new ArrayList<>();
        
        for (TransactionObject obj : lista) 
        {
            obj.setSystemId(systemId);
            
            listTransactions.add(obj);
        }
        
        return listTransactions;
    }
    
}
