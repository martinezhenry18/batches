/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoIncExtTransactionStatus.service;

import com.novo.NovoIncExtTransactionStatus.Objects.ApiResponseAutenticateObject;
import com.novo.NovoIncExtTransactionStatus.Objects.ExternalTransactionsObject;
import com.novo.NovoIncExtTransactionStatus.Objects.TransactionResponseObject;
import java.io.IOException;
import java.util.ArrayList;
import org.springframework.stereotype.Service;

/**
 *
 * @author emarmole
 */
public interface NovoIncExtNiceConsumeService 
{
    public ApiResponseAutenticateObject consumeWebRestService() throws IOException, Exception;
    
    public ArrayList<TransactionResponseObject> consumeTransactionApiFraud(ApiResponseAutenticateObject objAuth, ExternalTransactionsObject objExternalTransactions) throws IOException, Exception;
}
