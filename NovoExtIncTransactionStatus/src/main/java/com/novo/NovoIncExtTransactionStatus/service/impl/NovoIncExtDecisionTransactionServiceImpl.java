/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoIncExtTransactionStatus.service.impl;

import com.google.gson.Gson;
import com.novo.NovoIncExtTransactionStatus.Objects.ApiResponseDecisionTransactionObject;
import com.novo.NovoIncExtTransactionStatus.Objects.ApproveTransactionObject;
import com.novo.NovoIncExtTransactionStatus.Objects.GeneralParametersObject;
import com.novo.NovoIncExtTransactionStatus.Objects.RejectTransactionObject;
import com.novo.NovoIncExtTransactionStatus.Objects.TransactionDesicionObject;
import com.novo.NovoIncExtTransactionStatus.config.ConfigurationProperties;
import com.novo.NovoIncExtTransactionStatus.constant.HeaderParams;
import com.novo.NovoIncExtTransactionStatus.service.NovoIncExtDecisionTransactionService;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author emarmole
 */
@Service
public class NovoIncExtDecisionTransactionServiceImpl implements NovoIncExtDecisionTransactionService
{
    private static Logger logger = Logger.getLogger(NovoIncExtDecisionTransactionServiceImpl.class);
    
    @Override
    public ApiResponseDecisionTransactionObject approveTransaction(String idTransaction,TransactionDesicionObject objTransaction, String tenant)
    {
        DOMConfigurator.configure(System.getProperty("user.dir")+"/config/log4j.xml");
        ConfigurationProperties configurationProperties = new ConfigurationProperties();
        ApiResponseDecisionTransactionObject objResponse = new ApiResponseDecisionTransactionObject();
        try {
            ApproveTransactionObject objApprove = new ApproveTransactionObject();
            objApprove = (ApproveTransactionObject) configurationProperties.getConfig("approveParameters", objApprove, ApproveTransactionObject.class);
            
            String url = objApprove.getUrl()+ idTransaction + objApprove.getComplement();
            
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set(HeaderParams.TENANT, tenant);
            Gson gson = new Gson();
            
            HttpEntity<String> requestHttp = new HttpEntity<String>(gson.toJson(objTransaction), headers);
            
            ResponseEntity<ApiResponseDecisionTransactionObject> result = restTemplate.exchange(url, HttpMethod.valueOf(objApprove.getMethod()), requestHttp, ApiResponseDecisionTransactionObject.class);
            
            objResponse = result.getBody();
            
            
            return objResponse;
        } catch (Exception e) {
            logger.info("No se ha podido aprobar la transacción -> "+idTransaction);
            e.printStackTrace();
            objResponse.setRc("1");
            objResponse.setMsg("Fallo");
            return objResponse;
        }
    }
    
    @Override
    public ApiResponseDecisionTransactionObject rejectTransaction(String idTransaction,TransactionDesicionObject objTransaction, String tenant)
    {
        DOMConfigurator.configure(System.getProperty("user.dir")+"/config/log4j.xml");
        ConfigurationProperties configurationProperties = new ConfigurationProperties();
        ApiResponseDecisionTransactionObject objResponse = new ApiResponseDecisionTransactionObject();
        try {
            RejectTransactionObject objReject = new RejectTransactionObject();
            objReject = (RejectTransactionObject) configurationProperties.getConfig("rejectParameters", objReject, RejectTransactionObject.class);
            
            String url = objReject.getUrl();
            
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set(HeaderParams.TENANT, "zenus");
            Gson gson = new Gson();
            HttpEntity<String> requestHttp = new HttpEntity<String>(gson.toJson(objTransaction), headers);
            
            ResponseEntity<ApiResponseDecisionTransactionObject> result = restTemplate.exchange(url, HttpMethod.valueOf(objReject.getMethod()), requestHttp, ApiResponseDecisionTransactionObject.class);
            
            objResponse = result.getBody();
            
            
            return objResponse;
        } catch (Exception e) {
            logger.info("No se ha podido rechazar la transacción"+idTransaction);
            e.printStackTrace();
            objResponse.setRc("1");
            objResponse.setMsg("Fallo");
            return objResponse;
        }
    }
}
