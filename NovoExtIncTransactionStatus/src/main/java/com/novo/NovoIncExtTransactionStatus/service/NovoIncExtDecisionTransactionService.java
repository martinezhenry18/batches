/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoIncExtTransactionStatus.service;

import com.novo.NovoIncExtTransactionStatus.Objects.ApiResponseDecisionTransactionObject;
import com.novo.NovoIncExtTransactionStatus.Objects.TransactionDesicionObject;

/**
 *
 * @author emarmole
 */
public interface NovoIncExtDecisionTransactionService 
{
    public ApiResponseDecisionTransactionObject approveTransaction(String idTransaction,TransactionDesicionObject objTransaction, String tenant);
    public ApiResponseDecisionTransactionObject rejectTransaction(String idTransaction,TransactionDesicionObject objTransaction, String tenant);
}
