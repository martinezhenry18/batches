/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoIncExtTransactionStatus.service.impl;

import com.google.gson.Gson;
import com.novo.NovoIncExtTransactionStatus.Objects.ApiResponseDecisionTransactionObject;
import com.novo.NovoIncExtTransactionStatus.Objects.MsTeamsMessageObject;
import com.novo.NovoIncExtTransactionStatus.Objects.MsTeamsObject;
import com.novo.NovoIncExtTransactionStatus.Objects.NotificationConnectorContextObject;
import com.novo.NovoIncExtTransactionStatus.Objects.NotificationConnectorObject;
import com.novo.NovoIncExtTransactionStatus.config.ConfigurationProperties;
import com.novo.NovoIncExtTransactionStatus.constant.HeaderParams;
import com.novo.NovoIncExtTransactionStatus.service.NovoIncExtNotificationCenterService;
import java.io.IOException;
import java.util.Map;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author emarmole
 */
@Service
public class NovoIncExtNotificationCenterServiceImpl implements NovoIncExtNotificationCenterService
{
    private static Logger logger = Logger.getLogger(NovoIncExtNotificationCenterServiceImpl.class);

    @Override
    public void incExtTransactionsNotification(String mensaje, String type) throws IOException, Exception {
        DOMConfigurator.configure(System.getProperty("user.dir")+"/config/log4j.xml");
        logger.info("INIT PROCESS TO NOTIFICATION TEAMS");
        ConfigurationProperties configurationProperties = new ConfigurationProperties();
        
        try {
            MsTeamsObject objMsTeamsObject = new MsTeamsObject();
            objMsTeamsObject = (MsTeamsObject) configurationProperties.getConfig("msTeams", objMsTeamsObject, MsTeamsObject.class);
            if (objMsTeamsObject.isActive()) 
            {
                try {
                    RestTemplate restTemplate = new RestTemplate();
                    MsTeamsMessageObject request = new MsTeamsMessageObject();
                    HttpHeaders headers = new HttpHeaders();
                    switch (type)
                    {
                        case "error":
                            request.setThemeColor(objMsTeamsObject.getErrorColor());
                            request.setTitle(objMsTeamsObject.getErrorTitle());
                        break;
                        case "info":
                            request.setThemeColor(objMsTeamsObject.getInfoColor());
                            request.setTitle(objMsTeamsObject.getInfoTitle());
                        break;
                        case "success":
                            request.setThemeColor(objMsTeamsObject.getSuccessColor());
                            request.setTitle(objMsTeamsObject.getSuccessTitle());
                        break;
                    }
                    request.setText(mensaje);
                    headers.setContentType(MediaType.APPLICATION_JSON);
                    Gson gson = new Gson();
                    HttpEntity<String> requestHttp = new HttpEntity<String>(gson.toJson(request), headers);
                    String result = restTemplate.postForObject(objMsTeamsObject.getWebHook(), requestHttp, String.class);
                    if(result.compareTo("1")==0)
                        logger.info("Notification MST succesfull.");
                    else
                        logger.error("Notification MST failed. "+result);
                    
                } catch (Exception e) {
                    throw e;
                }
            }
        } catch (Exception e) {
            throw  e;
        }
    }

    @Override
    public void incExtTransactionsNotificationExternal(Map<String, Object> params, String tenant) throws IOException, Exception {
        DOMConfigurator.configure(System.getProperty("user.dir")+"/config/log4j.xml");
        logger.info("INIT PROCESS TO NOTIFICATION EMAIL");
        ConfigurationProperties configurationProperties = new ConfigurationProperties();
        
        try {
            NotificationConnectorObject objConnector = new NotificationConnectorObject();
            objConnector = (NotificationConnectorObject) configurationProperties.getConfig("notificationConnectorParameters", objConnector, NotificationConnectorObject.class);
            if (objConnector.isActive()) 
            {
                try {
                    RestTemplate restTemplate = new RestTemplate();
                    HttpHeaders headers = new HttpHeaders();
                    NotificationConnectorContextObject context = new NotificationConnectorContextObject();
                    ApiResponseDecisionTransactionObject objResponse = new ApiResponseDecisionTransactionObject();

                    Gson gson = new Gson();

                    String url = objConnector.getUrlNotification() + objConnector.getProveedorNotification();
                    headers.setContentType(MediaType.APPLICATION_JSON);
                    headers.set(HeaderParams.TENANT, tenant);


                    params.put("country-send", objConnector.getCountrySend());
                    context.setTo(objConnector.getEmailNotification());
                    context.setBody(objConnector.getTemplateNotification());
                    context.setSubject(objConnector.getSubject());
                    context.setAppIdentifier(objConnector.getAppIdentifier());
                    context.setCc(objConnector.getCc());
                    context.setChannel(objConnector.getChannel());
                    context.setVariableData(params);

                    HttpEntity requestHttp = new HttpEntity<String>(gson.toJson(context), headers);
                    String result = restTemplate.postForObject(url, requestHttp, String.class);

                    objResponse = gson.fromJson(result, ApiResponseDecisionTransactionObject.class);

                    if(objResponse.getRc().equals("0"))
                        logger.info("Notification ConnectorMail succesfull.");
                    else
                        logger.error("Notification ConnectorMail failed. "+result);
                } catch (Exception ex) {
                    logger.error("Notification ConnectorMail failed. "+ex);
                }
            }
        } catch (Exception e) {
        }
    }
    
}
