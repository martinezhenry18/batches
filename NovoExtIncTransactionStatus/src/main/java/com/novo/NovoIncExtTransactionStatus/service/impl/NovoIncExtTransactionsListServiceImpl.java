/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoIncExtTransactionStatus.service.impl;

import com.novo.NovoIncExtTransactionStatus.Objects.TransactionObject;
import com.novo.NovoIncExtTransactionStatus.repository.NovoIncExtTransactionsListRepository;
import com.novo.NovoIncExtTransactionStatus.service.NovoIncExtTransactionsListService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author emarmole
 */
@Service
public class NovoIncExtTransactionsListServiceImpl implements NovoIncExtTransactionsListService
{

    @Autowired
    private NovoIncExtTransactionsListRepository novoIncExtTransactionsListRepository;
    
    @Override
    public ArrayList<TransactionObject> getListTransactions() throws Exception {
        return novoIncExtTransactionsListRepository.getListTransactions();
    }
    
}
