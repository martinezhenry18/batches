/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoIncExtTransactionStatus.constant;

/**
 *
 * @author emarmole
 */
public enum TransactionPrefix {
    EXTERNAL_IN_ACH("ACH","EIA"),
    EXTERNAL_IN_WIRE("WIRE","EIW");
    
    private String type;
    private String prefix;

    private TransactionPrefix(String type, String prefix) {
        this.type = type;
        this.prefix = prefix;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    
    public static String getPrefixByType(String type) {
        for (TransactionPrefix e : values()) {
            if (e.type.equals(type)) {
                return e.prefix;
            }
        }
        return "WIRE";
    }
    
}
