package com.novo.NovoIncExtTransactionStatus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class NovoIncExtTransactionStatusApplication {
            
    public static void main(String[] args) throws Exception {
        SpringApplication.run(NovoIncExtTransactionStatusApplication.class, args);
    }

}
