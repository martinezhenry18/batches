/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoIncExtTransactionStatus.Objects;

/**
 *
 * @author emarmole
 */
public class ProcedureDbObject 
{
    private String dbPackage;
    private String dbProcedure;

    public String getDbPackage() {
        return dbPackage;
    }

    public void setDbPackage(String dbPackage) {
        this.dbPackage = dbPackage;
    }

    public String getDbProcedure() {
        return dbProcedure;
    }

    public void setDbProcedure(String dbProcedure) {
        this.dbProcedure = dbProcedure;
    }
    
    
}
