/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoIncExtTransactionStatus.Objects;

/**
 *
 * @author emarmole
 */
public class ReportContExtTransactionObject 
{
    private Integer totalTransactions;
    private Integer holdTransactions;
    private Integer cancelTransactions;
    private Integer releaseTransactions;
    private Integer unknownTransactions;
    private Integer failedApproveTransactions;
    private Integer failedCancelTransactions;

    public ReportContExtTransactionObject() {
        this.setCancelTransactions(0);
        this.setFailedApproveTransactions(0);
        this.setFailedCancelTransactions(0);
        this.setHoldTransactions(0);
        this.setReleaseTransactions(0);
        this.setTotalTransactions(0);
        this.setUnknownTransactions(0);
    }
    
    

    public Integer getFailedApproveTransactions() {
        return failedApproveTransactions;
    }

    public void setFailedApproveTransactions(Integer failedApproveTransactions) {
        this.failedApproveTransactions = failedApproveTransactions;
    }

    public Integer getFailedCancelTransactions() {
        return failedCancelTransactions;
    }

    public void setFailedCancelTransactions(Integer failedCancelTransactions) {
        this.failedCancelTransactions = failedCancelTransactions;
    }
    
    public Integer getTotalTransactions() {
        return totalTransactions;
    }

    public void setTotalTransactions(Integer totalTransactions) {
        this.totalTransactions = totalTransactions;
    }

    public Integer getHoldTransactions() {
        return holdTransactions;
    }

    public void setHoldTransactions(Integer holdTransactions) {
        this.holdTransactions = holdTransactions;
    }

    public Integer getCancelTransactions() {
        return cancelTransactions;
    }

    public void setCancelTransactions(Integer cancelTransactions) {
        this.cancelTransactions = cancelTransactions;
    }

    public Integer getReleaseTransactions() {
        return releaseTransactions;
    }

    public void setReleaseTransactions(Integer releaseTransactions) {
        this.releaseTransactions = releaseTransactions;
    }

    public Integer getUnknownTransactions() {
        return unknownTransactions;
    }

    public void setUnknownTransactions(Integer unknownTransactions) {
        this.unknownTransactions = unknownTransactions;
    }
    
    
}
