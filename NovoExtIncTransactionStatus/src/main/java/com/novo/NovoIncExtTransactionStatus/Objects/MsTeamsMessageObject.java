/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novo.NovoIncExtTransactionStatus.Objects;

/**
 *
 * @author emarmole
 */
public class MsTeamsMessageObject 
{
    private String text;
    private String title;
    private String themeColor;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThemeColor() {
        return themeColor;
    }

    public void setThemeColor(String themeColor) {
        this.themeColor = themeColor;
    }

    public MsTeamsMessageObject(String text, String title, String themeColor) {
        super();
        this.text = text;
        this.title = title;
        this.themeColor = themeColor;
    }
    
    public MsTeamsMessageObject() {
        super();
    }
    
}
